import 'package:flutter/material.dart';
import 'package:lecture_practice_5/pages/second_page.dart';
import 'package:lecture_practice_5/provider/data_provider.dart';
import 'package:provider/provider.dart';

class FirstPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var counter = Provider.of<DataProvider>(context).counter;
    return Scaffold(
      backgroundColor: Colors.blueGrey,
      appBar: AppBar(
        actions: [
          IconButton(
            iconSize: 40,
            icon: Icon(Icons.arrow_right),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (ctx) => SecondPage(),
                ),
              );
            },
          ),
        ],
      ),
      body: Center(
        child: Container(
          color: Colors.lightBlue[100],
          height: 100,
          width: 100,
          alignment: Alignment.center,
          child: FittedBox(
            child: Text(
              '$counter',
              style: TextStyle(fontSize: 40),
            ),
          ),
        ),
      ),
    );
  }
}
