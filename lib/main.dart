import 'package:flutter/material.dart';
import 'package:lecture_practice_5/pages/first_page.dart';
import 'package:lecture_practice_5/provider/data_provider.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => DataProvider(),
      child: MaterialApp(
        home: FirstPage(),
      ),
    );
  }
}
